/// @file
/// @brief Simple operation with files

//
// Created by vlad on 3/4/23.
//

#include "utils.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// \brief Parse cl args into structure
/// \param argc Number of args
/// \param argv Args array
/// \param parameters Structure to store args
/// \return Boolean - success of parsing
bool parse_params(int argc, char** argv, struct ProgramParameters* parameters) {
    if (argc != 4) {
        return false;
    }
    parameters->pe_file = argv[1];
    parameters->section_name = argv[2];
    parameters->out_bimage = argv[3];
    return true;
}

/// \brief Print error message and exit with special code
/// \param message Error message
/// \param code Exit code
void throw_exception(const char* message, int code) {
    perror("Errno error message ");
    if (message != NULL)
        printf("Internal info: %s; code: %d\n", message, code);
    exit(code);
}
