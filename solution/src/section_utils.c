/// @file
//
// Created by vlad on 3/4/23.
//

#include "section_utils.h"
#include "utils.h"
#include <malloc.h>
#include <string.h>

/// \brief Looking up section with special name
/// \param name Name to find
/// \param peFile Structure with section headers
/// \return Address of the SectionHeader or NULL if not found
struct SectionHeader* find_section(char* name, struct PEFile* peFile) {
    for (uint32_t i = 0; i < peFile->header->number_of_sections; i++) {
        if (strcmp((char*)peFile->section_headers[i].name, name) == 0)
            return peFile->section_headers + i;
    }
    return NULL;
}

/// \brief Read section data
/// \param file Filestream to read
/// \param header Header of section
/// \return Structure with section data and its size
struct Section read_section(FILE* file, struct SectionHeader* header) {
    struct Section section = {
            .size = header->rawdata_size,
            .data = malloc(header->rawdata_size)
    };
    if (fseek(file, header->pointer_to_rawdata, SEEK_SET))
        throw_exception("Error while working with section data", 1);
    if (!fread(section.data, section.size, 1, file))
        throw_exception("Error while reading section", 1);
    return section;
}

/// \brief free memory allocated for section data
void free_section(struct Section section) {
    free(section.data);
}
