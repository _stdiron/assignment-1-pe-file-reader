/// @file
//
// Created by vlad on 11/18/22.
//

#include "file.h"
#include <errno.h>
#include <stdlib.h>

///
/// @param file Filestream to define
/// @param mode Writing mode
/// @param path Path to file
/// @return Enum value of a success
enum open_file_status open_file(FILE** file, const char* mode, const char* path) {
    errno = 0;
    *file = fopen(path, mode);
    if (*file == NULL || errno != 0)
        return OPEN_ERROR;
    return OPEN_SUCCESS;
}
///
/// @param file Filestream to close
/// @return Enum value of a success
enum close_file_status close_file(FILE** file) {
    if (fclose(*file))
        return CLOSE_ERROR;
    return CLOSE_SUCCESS;
}
