/// @file 
/// @brief Main application file

#include "file.h"
#include "pe_file.h"
#include "pe_reader.h"
#include "section_utils.h"
#include "utils.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    struct ProgramParameters parameters = {0};
    if (!parse_params(argc, argv, &parameters)) {
        usage(stdout);
        return 0;
    }

    FILE* in_file = NULL;
    if (open_file(&in_file, "rb", parameters.pe_file) != OPEN_SUCCESS)
        throw_exception("Error while opening pe file", 1);
    struct PEFile pe_file = {0};

    read_pe_file(in_file, &pe_file);
    struct SectionHeader* found_header = find_section(parameters.section_name, &pe_file);
    if (found_header == NULL)
        throw_exception("Section with specified name not found", 1);
    struct Section section = read_section(in_file, found_header);

    FILE* out_file = NULL;
    if (open_file(&out_file, "wb", parameters.out_bimage) != OPEN_SUCCESS)
        throw_exception("Error while creating output file", 1);
    if (!fwrite(section.data, section.size, 1, out_file))
        throw_exception("Error while writing section in file", 1);

    close_file(&in_file);
    close_file(&out_file);
    free_pe_struct(&pe_file);
    free_section(section);

    return 0;
}
