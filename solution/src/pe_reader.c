/// @file
//
// Created by vlad on 3/4/23.
//

#include "pe_reader.h"
#include "stdbool.h"
#include "utils.h"
#include <malloc.h>

/// @brief Read pe-header into PEHeader structure inside PEFile
/// @param file Filestream to read data
/// @param pe_file Pointer to the PEFile to write data
/// @return Boolean - success of operation
static bool read_pe_header(FILE* file, struct PEFile* pe_file) {
    if (fseek(file, pe_file->header_offset, SEEK_SET))
        return false;
    if (!fread(pe_file->header, sizeof(struct PEHeader), 1, file))
        return false;
    return true;
}

/// @brief Read all section headers into SectionHeader structure inside PEFile
/// @param file Filestream to read data
/// @param pe_file Pointer to the PEFile to write data
/// @return Boolean - success of operation
static bool read_section_headers(FILE* file, struct PEFile* pe_file) {
    if (fseek(file, pe_file->section_header_offset, SEEK_SET))
        return false;
    if (fread(
            pe_file->section_headers,
            sizeof(struct SectionHeader),
            pe_file->header->number_of_sections,
            file
        ) != pe_file->header->number_of_sections)
        return false;
    return true;
}

/// @brief Main function in module. Malloc memory for headers and read it from filestream into there. All pointers & data saves into PEFile.
/// @param file Filestream to read data
/// @param pe_file Pointer to structure that stores headers and offsets.
void read_pe_file(FILE* file, struct PEFile* pe_file) {
    if (fseek(file, HEADER_OFFSET_ADDR, SEEK_SET) == -1)
         throw_exception("Error while working with filestream", 1);
    if (!fread(&pe_file->header_offset, 4, 1, file))
        throw_exception("Error while reading header offset", 1);

    pe_file->header = malloc(sizeof(struct PEHeader));
    if (!read_pe_header(file, pe_file))                                                // TODO: improve error feedback
        return;

    pe_file->optional_header_offset = pe_file->header_offset + sizeof(struct PEHeader);
    pe_file->section_header_offset = pe_file->optional_header_offset + pe_file->header->size_of_optional_header;
    pe_file->section_headers = malloc(sizeof(struct SectionHeader) * pe_file->header->number_of_sections);

    if (!read_section_headers(file, pe_file))
        throw_exception("Error while reading section headers", 1);
}

/// @brief Free memory allocated for headers
void free_pe_struct(struct PEFile* pe_file) {
    free(pe_file->header);
    free(pe_file->section_headers);
}
