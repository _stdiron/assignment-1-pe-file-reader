//
// Created by vlad on 3/4/23.
//

#ifndef ASSIGNMENT_1_PE_FILE_READER_UTILS_H
#define ASSIGNMENT_1_PE_FILE_READER_UTILS_H

#include <stdbool.h>
#include <stdint.h>

/// @brief Structure to group program cl arguments
struct ProgramParameters {
    /// path to the source file
    char* pe_file;
    /// name of desired section
    char* section_name;
    /// path to the output file
    char* out_bimage;
};

bool parse_params(int argc, char** argv, struct ProgramParameters* parameters);
void throw_exception(const char* message, int code);

#endif //ASSIGNMENT_1_PE_FILE_READER_UTILS_H
