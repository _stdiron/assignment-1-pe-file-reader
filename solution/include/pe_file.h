//
// Created by vlad on 3/4/23.
//

#ifndef ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H
#define ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H

#include <stdint.h>

#define HEADER_OFFSET_ADDR 0x3c

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
PEHeader {
    /// Magic value
    uint8_t magic[4];
    /// Type of target machine
    uint16_t machine;
    /// Number of sections
    uint16_t number_of_sections;
    /// File was created (in sec)
    uint32_t time_data_stamp;
    /// Offset of the COFF symbol table
    uint32_t pointer_to_symbol_table;
    /// Number of entries in the symbol table
    uint32_t number_of_symbols;
    /// Size of the optional header
    uint16_t size_of_optional_header;
    /// The flags that indicate the attributes of the file
    uint16_t characteristics;
};

struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
SectionHeader {
    /// UTF-8 encoded name of section
    uint8_t name[8];
    /// The total size of the section when loaded into memory
    uint32_t virtual_size;
    /// The address of the section relative to the image base when loaded into memory
    uint32_t virtual_address;
    /// The size of the section
    uint32_t rawdata_size;
    /// The file pointer to the first page of the section
    uint32_t pointer_to_rawdata;
    /// The file pointer to the beginning of relocation entries
    uint32_t pointer_to_relocations;
    /// The file pointer to the beginning of line-number entries
    uint32_t pointer_to_line_numbers;
    /// The number of relocation entries for the section
    uint16_t number_of_relocations;
    /// The number of line-number entries for the sectio
    uint16_t number_of_line_numbers;
    /// The flags that describe the characteristics of the section
    uint32_t characteristics;
};

#ifdef _MSC_VER
#pragma pop()
#endif

/// @brief Structure to merge all the data about pe-file
struct PEFile {
    /// @name Offsets within file
    ///@{
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to an optional header
    uint32_t optional_header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    ///@{
    /// Main header
    struct PEHeader* header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader* section_headers;
    ///@}

};

#endif //ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H
