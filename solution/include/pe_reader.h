//
// Created by vlad on 3/4/23.
//

#ifndef ASSIGNMENT_1_PE_FILE_READER_PE_READER_H
#define ASSIGNMENT_1_PE_FILE_READER_PE_READER_H

#include "pe_file.h"
#include <stdio.h>

void read_pe_file(FILE* file, struct PEFile* pe_file);
void free_pe_struct(struct PEFile* pe_file);

#endif //ASSIGNMENT_1_PE_FILE_READER_PE_READER_H
