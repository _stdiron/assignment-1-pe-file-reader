//
// Created by vlad on 3/4/23.
//

#ifndef ASSIGNMENT_1_PE_FILE_READER_SECTION_UTILS_H
#define ASSIGNMENT_1_PE_FILE_READER_SECTION_UTILS_H

#include "file.h"
#include "pe_file.h"
#include "stdint.h"

/// @brief Structure with raw section data and its size
struct Section {
    /// data size
    uint32_t size;
    /// raw data
    uint8_t* data;
};

struct SectionHeader* find_section(char* name, struct PEFile* peFile);
struct Section read_section(FILE* file, struct SectionHeader* header);
void free_section(struct Section section);

#endif //ASSIGNMENT_1_PE_FILE_READER_SECTION_UTILS_H
